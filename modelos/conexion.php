<?php

class Conexion{

	static public function conectar(){

		$link = new PDO("mysql:host=localhost;dbname=php_api_udemy_db", "php_api_udemy_db", "php_api_udemy_db");

		$link->exec("set names utf8");

		return $link;

	}

}