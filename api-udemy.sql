-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-11-2018 a las 00:10:50
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api-udemy`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` text COLLATE utf8_spanish_ci NOT NULL,
  `api` text COLLATE utf8_spanish_ci NOT NULL,
  `ruta` text COLLATE utf8_spanish_ci NOT NULL,
  `titulo` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `palabrasClaves` text COLLATE utf8_spanish_ci NOT NULL,
  `icono` text COLLATE utf8_spanish_ci NOT NULL,
  `imgOferta` text COLLATE utf8_spanish_ci NOT NULL,
  `imgBanner` text COLLATE utf8_spanish_ci NOT NULL,
  `oferta` float NOT NULL,
  `descuento` int(11) NOT NULL,
  `finOferta` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `api`, `ruta`, `titulo`, `descripcion`, `palabrasClaves`, `icono`, `imgOferta`, `imgBanner`, `oferta`, `descuento`, `finOferta`) VALUES
(1, 'Desarrollo', 'Development', 'development', 'Cursos en línea de Desarrollo | Tutoriales a tu Alcance', 'Aprenda a codificar o crear sitios web desde cero con estos cursos en línea. Los temas incluyen desarrollo web, aplicaciones móviles IOS, desarrollo de Android a juegos y comercio electrónico.', 'aprender, codificar, crear, sitios web, cursos en línea, desarrollo web, aplicaciones móviles, IOS, Android, juegos, comercio electrónico, gratis, gratuito', 'fas fa-code', 'vistas/img/ofertas/desarrollo.jpg', 'vistas/img/banner/desarrollo.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(2, 'Negocios', 'Business', 'business', 'Cursos en línea de Negocios | Tutoriales a tu Alcance', 'La mejor colección de cursos de negocios y emprendimiento en línea. Desde iniciar una empresa hasta marketing, publicidad, finanzas y más. Comienza a aprender hoy.', 'colección, cursos de negocios, emprendimiento en línea, iniciar una empresa, marketing, publicidad, finanzas, gratis, gratuito', 'fas fa-chart-line', 'vistas/img/ofertas/business.jpg', 'vistas/img/banner/business.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(3, 'Software', 'IT%20%26%20Software', 'it-and-software', 'Cursos en línea de Informática y software | Tutoriales a tu Alcance', 'Los últimos cursos en línea de informática y software. Prepárese para los exámenes de certificación o estudie las últimas arquitecturas de sistemas operativos.', 'cursos en línea de informática, software, exámenes de certificación, últimas arquitecturas, sistemas operativos, gratis, gratuito', 'fas fa-laptop', 'vistas/img/ofertas/it-y-software.jpg', 'vistas/img/banner/it-y-software.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(4, 'Oficina', 'Office%20Productivity', 'office-productivity', 'Cursos en línea de Productividad en la oficina | Tutoriales a tu Alcance', 'Mejore sus resultados y avance con estos cursos de productividad de oficina en: Microsoft Excel, Apple, Google, SAP, Intuit, Oracle, Salesforce y otros.', 'cursos de productividad, cursos de oficina, Microsoft Excel, Apple, Google, SAP, Intuit, Oracle, Salesforce, gratis, gratuito', 'fas fa-briefcase', 'vistas/img/ofertas/oficina.jpg', 'vistas/img/banner/oficina.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(5, 'D. Personal', 'Personal%20Development', 'personal-development', 'Cursos en línea de Desarrollo personal | Tutoriales a tu Alcance', 'Con los cursos de desarrollo personal en línea, aprenderá a ser más seguro, más feliz, productivo y exitoso. Regístrese hoy y comience a aprender.', 'cursos de desarrollo personal, seguridad, felicidad, productividad, éxito, gratis, gratuito', 'fas fa-book', 'vistas/img/ofertas/personal.jpg', 'vistas/img/banner/personal.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(6, 'Diseño', 'Design', 'design', 'Cursos en línea de Diseño | Tutoriales a tu Alcance', 'Los mejores cursos de diseño en línea: aprenda a diseñar sitios web, logotipos, carteles y más. Encuentre un curso y comience a aprender hoy.', 'cursos de diseño, diseñar sitios web, logotipos, carteles, gratis, gratuito', 'fas fa-pen-square', 'vistas/img/ofertas/diseno.jpg', 'vistas/img/banner/diseno.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(7, 'Marketing', 'Marketing', 'marketing', 'Cursos en línea de Marketing en redes sociales y correo electrónico | Tutoriales a tu Alcance', 'Aprenda marketing para hacer crecer su negocio. Desarrolle su marketing digital, redes sociales, hacking de crecimiento, contenido y habilidades de marca con estos excelentes cursos.', 'Aprenda marketing, crecer su negocio, marketing digital, redes sociales, hacking de crecimiento, contenido y habilidades de marca, gratis, gratuito', 'fas fa-users', 'vistas/img/ofertas/marketing.jpg', 'vistas/img/banner/marketing.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(8, 'Música', 'Music', 'music', 'Cursos en línea de Música | Tutoriales a tu Alcance', 'Aprende música en línea. Los mejores cursos en línea de guitarra, piano, ukelele, autoharp y DJ. Descubre un curso y comienza a aprender.', 'Aprende música en línea, los mejores cursos, guitarra, piano, ukelele, autoharp, DJ, gratis, gratuito', 'fas fa-music', 'vistas/img/ofertas/music.jpg', 'vistas/img/banner/music.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(9, 'Salud y Fitness', 'Health%20%26%20Fitness', 'health-and-fitness', 'Cursos en línea de Yoga, fitness y salud | Tutoriales a tu Alcance', 'Obtenga más salud y ajuste con los cursos de fitness en línea. Aprenda yoga, pilates, meditación, tai chi y más. Encuentra un curso y comienza a transformar tu cuerpo.', 'Obtenga más salud, cursos de fitness en línea, Aprenda yoga, pilates, meditación, tai chi, gratis, gratuito', 'fas fa-heartbeat', 'vistas/img/ofertas/salud.jpg', 'vistas/img/banner/salud.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(10, 'Fotografía', 'Photography', 'photography', 'Cursos de Fotografía | Tutoriales a tu Alcance', 'Lleva tus habilidades de fotografía al siguiente nivel. Regístrese en nuestros cursos de fotografía en línea gratuitos o de bajo costo y comience a tomar impresionantes fotos hoy.', 'fotografía en línea, cursos de fotografía, gratis, gratuito', 'fas fa-camera', 'vistas/img/ofertas/fotografia.jpg', 'vistas/img/banner/fotografia.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(11, 'Estilo de vida', 'Lifestyle', 'lifestyle', 'Cursos en línea de Estilo de vida | Tutoriales a tu Alcance', 'Aprende cómo cocinar, entrenar a tu cachorro, conseguir un trabajo de ensueño y mucho más.', 'Aprende cómo cocinar, entrenar a tu cachorro, conseguir un trabajo de ensueño, gratis, gratuito', 'fas fa-thumbs-up', 'vistas/img/ofertas/lifestyle.jpg', 'vistas/img/banner/lifestyle.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(12, 'Idiomas', 'Language', 'language', 'Cursos en línea de Aprendizaje de idiomas | Tutoriales a tu Alcance', 'Con los cursos de idiomas en línea, puede dominar las habilidades que son importantes para usted de forma gratuita o a bajo costo desde su dispositivo móvil, tableta o computadora portátil.', 'cursos de idiomas en línea, dominar las habilidades, forma gratuita, dispositivo móvil, tableta,computadora portátil, gratis, gratuito', 'fas fa-globe', 'vistas/img/ofertas/idiomas.jpg', 'vistas/img/banner/idiomas.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(13, 'Formación de Profesorado', 'Teacher%20Training', 'teacher-training', 'Cursos en línea de Formación de Profesorado | Tutoriales a tu Alcance', 'Con los cursos de educación en línea, puede dominar las habilidades que son importantes para usted de forma gratuita o a bajo costo desde su dispositivo móvil, tableta o computadora portátil.', 'cursos de formación de profesorado en línea, dominar las habilidades, forma gratuita, dispositivo móvil, tableta,computadora portátil, gratis, gratuito', 'fas fa-chalkboard-teacher', 'vistas/img/ofertas/profesorado.jpg', 'vistas/img/banner/profesorado.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(14, 'Estudios Académicos', 'Academics', 'academics', 'Cursos en línea de Estudios Académicos| Tutoriales a tu Alcance', 'Tome cursos en línea en matemáticas, ciencia, geografía e historia de expertos.', 'cursos de estudios académicos en línea, dominar las habilidades, forma gratuita, dispositivo móvil, tableta,computadora portátil, gratis, gratuito', 'fab fa-react', 'vistas/img/ofertas/academia.jpg', 'vistas/img/banner/academia.jpg', 11.99, 0, '2018-11-15 23:59:59'),
(15, 'Preparación para Exámenes', 'Test%20Prep', 'test-prep', 'Cursos en línea de Preparación para Exámenes | Tutoriales a tu Alcance', 'Con los cursos de preparación para exámenes en línea, aprende cómo superar exitósamente esa prueba.', 'cursos de preparación para exámenes en línea, dominar las habilidades, forma gratuita, dispositivo móvil, tableta,computadora portátil, gratis, gratuito', 'fas fa-file-alt', 'vistas/img/ofertas/examenes.jpg', 'vistas/img/banner/examenes.jpg', 11.99, 0, '2018-11-15 23:59:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL,
  `subcategoria` text COLLATE utf8_spanish_ci NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `api` text COLLATE utf8_spanish_ci NOT NULL,
  `ruta` text COLLATE utf8_spanish_ci NOT NULL,
  `oferta` float NOT NULL,
  `descuento` int(11) NOT NULL,
  `finOferta` datetime NOT NULL,
  `gratis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `subcategoria`, `id_categoria`, `api`, `ruta`, `oferta`, `descuento`, `finOferta`, `gratis`) VALUES
(1, 'Cursos gratis de Desarrollo', 1, 'Development', 'development-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(2, 'Desarrollo web', 1, 'Web%20Development', 'web-development', 11.99, 0, '2018-11-15 23:59:59', 0),
(3, 'Aplicaciones móviles', 1, 'Mobile%20Apps', 'mobile-apps', 11.99, 0, '2018-11-15 23:59:59', 0),
(4, 'Lenguajes de programación', 1, 'Programming%20Languages', 'programming-languages', 11.99, 0, '2018-11-15 23:59:59', 0),
(5, 'Desarrollo de videoluegos', 1, 'Game%20Development', 'game-development', 11.99, 0, '2018-11-15 23:59:59', 0),
(6, 'Bases de datos', 1, 'Databases', 'databases', 11.99, 0, '2018-11-15 23:59:59', 0),
(7, 'Testeo de software', 1, 'Software%20Testing', 'software-testing', 11.99, 0, '2018-11-15 23:59:59', 0),
(8, 'Ingeniería de software', 1, 'Software%20Engineering', 'software-engineering', 11.99, 0, '2018-11-15 23:59:59', 0),
(9, 'Herramientas de desarrollo', 1, 'Development%20Tools', 'development-tools', 11.99, 0, '2018-11-15 23:59:59', 0),
(10, 'Comercio electrónico', 1, 'E-Commerce', 'e-commerce', 11.99, 0, '2018-11-15 23:59:59', 0),
(11, 'Cursos gratis de Negocios', 2, 'Business', 'business-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(12, 'Finanzas', 2, 'Finance', 'finance', 11.99, 0, '2018-11-15 23:59:59', 0),
(13, 'Emprendimiento', 2, 'Entrepreneurship', 'entrepreneurship', 11.99, 0, '2018-11-15 23:59:59', 0),
(14, 'Comunicación', 2, 'Communications', 'communications', 11.99, 0, '2018-11-15 23:59:59', 0),
(15, 'Gestión empresarial', 2, 'Management', 'management', 11.99, 0, '2018-11-15 23:59:59', 0),
(16, 'Ventas', 2, 'Sales', 'sales', 11.99, 0, '2018-11-15 23:59:59', 0),
(17, 'Estrategia', 2, 'Strategy', 'strategy', 11.99, 0, '2018-11-15 23:59:59', 0),
(18, 'Operaciones', 2, 'Operations', 'operations', 11.99, 0, '2018-11-15 23:59:59', 0),
(19, 'Gestión de proyectos', 2, 'Project%20Management', 'project-management', 11.99, 0, '2018-11-15 23:59:59', 0),
(20, 'Derecho empresarial', 2, 'Business%20Law', 'business-law', 11.99, 0, '2018-11-15 23:59:59', 0),
(21, 'Datos y análisis', 2, 'Data%20%26%20Analytics', 'data-and-analytics', 11.99, 0, '2018-11-15 23:59:59', 0),
(22, 'Negocios desde casa', 2, 'Home%20Business', 'home-business', 11.99, 0, '2018-11-15 23:59:59', 0),
(23, 'Recursos humanos', 2, 'Human%20Resources', 'human-resources', 11.99, 0, '2018-11-15 23:59:59', 0),
(24, 'Industria', 2, 'Industry', 'industry', 11.99, 0, '2018-11-15 23:59:59', 0),
(25, 'Medios de comunicación', 2, 'Media', 'media', 11.99, 0, '2018-11-15 23:59:59', 0),
(26, 'Bienes inmuebles', 2, 'Real%20Estate', 'real-estate', 11.99, 0, '2018-11-15 23:59:59', 0),
(27, 'Cursos gratis de IT y Software', 3, 'IT%20%26%20Software', 'it-and-software-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(28, 'Certificaciones de IT', 3, 'IT%20Certification', 'it-certification', 11.99, 0, '2018-11-15 23:59:59', 0),
(29, 'Redes y seguridad', 3, 'Network%20%26%20Security', 'network-and-security', 11.99, 0, '2018-11-15 23:59:59', 0),
(30, 'Hardware', 3, 'Hardware', 'hardware', 11.99, 0, '2018-11-15 23:59:59', 0),
(31, 'Sistemas operativos', 3, 'Operating%20Systems', 'operating-systems', 11.99, 0, '2018-11-15 23:59:59', 0),
(32, 'Cursos gratis de Productividad', 4, 'Office%20Productivity', 'office-productivity-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(33, 'Microsoft', 4, 'Microsoft', 'microsoft', 11.99, 0, '2018-11-15 23:59:59', 0),
(34, 'Apple', 4, 'Apple', 'apple', 11.99, 0, '2018-11-15 23:59:59', 0),
(35, 'Google', 4, 'Google', 'google', 11.99, 0, '2018-11-15 23:59:59', 0),
(36, 'SAP', 4, 'SAP', 'sap', 11.99, 0, '2018-11-15 23:59:59', 0),
(37, 'Intuit', 4, 'Intuit', 'intuit', 11.99, 0, '2018-11-15 23:59:59', 0),
(38, 'Salesforce', 4, 'Salesforce', 'salesforce', 11.99, 0, '2018-11-15 23:59:59', 0),
(39, 'Oracle', 4, 'Oracle', 'oracle', 11.99, 0, '2018-11-15 23:59:59', 0),
(40, 'Cursos gratis de Desarrollo personal', 5, 'Personal%20Development', 'personal-development-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(41, 'Transformación personal', 5, 'Personal%20Transformation', 'personal-transformation', 11.99, 0, '2018-11-15 23:59:59', 0),
(42, 'Productividad', 5, 'Productivity', 'productivity', 11.99, 0, '2018-11-15 23:59:59', 0),
(43, 'Liderazgo', 5, 'Leadership', 'leadership', 11.99, 0, '2018-11-15 23:59:59', 0),
(44, 'Finanzas personales', 5, 'Personal%20Finance', 'personal-finance', 11.99, 0, '2018-11-15 23:59:59', 0),
(45, 'Desarrollo profesional', 5, 'Career%20Development', 'career-development', 11.99, 0, '2018-11-15 23:59:59', 0),
(46, 'Paternidad y relaciones', 5, 'Parenting%20%26%20Relationships', 'parenting-and-relationships', 11.99, 0, '2018-11-15 23:59:59', 0),
(47, 'Felicidad', 5, 'Happiness', 'happiness', 11.99, 0, '2018-11-15 23:59:59', 0),
(48, 'Religión y espiritualidad', 5, 'Religion%20%26%20Spirituality', 'religion-and-spirituality', 11.99, 0, '2018-11-15 23:59:59', 0),
(49, 'Desarrollo de marca personal', 5, 'Personal%20Brand%20Building', 'personal-brand-building', 11.99, 0, '2018-11-15 23:59:59', 0),
(50, 'Creatividad', 5, 'Creativity', 'creativity', 11.99, 0, '2018-11-15 23:59:59', 0),
(51, 'influencia', 5, 'Influence', 'influence', 11.99, 0, '2018-11-15 23:59:59', 0),
(52, 'Autoestima', 5, 'Self%20Esteem', 'self-esteem', 11.99, 0, '2018-11-15 23:59:59', 0),
(53, 'Gestión del estrés', 5, 'Stress%20Management', 'stress-management', 11.99, 0, '2018-11-15 23:59:59', 0),
(54, 'Memoria y técnicas de estudio', 5, 'Memory%20%26%20Study%20Skills', 'memory-and-study-skills', 11.99, 0, '2018-11-15 23:59:59', 0),
(55, 'Motivación', 5, 'Motivation', 'motivation', 11.99, 0, '2018-11-15 23:59:59', 0),
(56, 'Cursos gratis de Diseño', 6, 'Design', 'design-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(57, 'Diseño web', 6, 'Web%20Design', 'web-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(58, 'Diseño gáfico', 6, 'Graphic%20Design', 'graphic-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(59, 'Herramientas de diseño', 6, 'Design%20Tools', 'design-tools', 11.99, 0, '2018-11-15 23:59:59', 0),
(60, 'Experiencia de usuario', 6, 'User%20Experience', 'user-experience', 11.99, 0, '2018-11-15 23:59:59', 0),
(61, 'Diseño de juegos', 6, 'Game%20Design', 'game-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(62, 'Desing Thinking', 6, 'Design%20Thinking', 'design-thinking', 11.99, 0, '2018-11-15 23:59:59', 0),
(63, '3D y animación', 6, '3D%20%26%20Animation', '3d-and-animation', 11.99, 0, '2018-11-15 23:59:59', 0),
(64, 'Moda', 6, 'Fashion', 'fashion', 11.99, 0, '2018-11-15 23:59:59', 0),
(65, 'Diseño arquitectónico', 6, 'Architectural%20Design', 'architectural-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(66, 'Diseño de interiores', 6, 'Interior%20Design', 'interior-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(67, 'Cursos gratis de Marketing', 7, 'Marketing', 'marketing-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(68, 'Marketing digital', 7, 'Digital%20Marketing', 'digital-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(69, 'Optimización de motores de búsqueda', 7, 'Search%20Engine%20Optimization', 'search-engine-optimization', 11.99, 0, '2018-11-15 23:59:59', 0),
(70, 'Marketing de redes sociales', 7, 'Social%20Media%20Marketing', 'social-media-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(71, 'Creación de marca', 7, 'Branding', 'branding', 11.99, 0, '2018-11-15 23:59:59', 0),
(72, 'Fundamentos de marketing', 7, 'Marketing%20Fundamentals', 'marketing-fundamentals', 11.99, 0, '2018-11-15 23:59:59', 0),
(73, 'Análisis y automatización', 7, 'Analytics%20%26%20Automation', 'analytics-and-automation', 11.99, 0, '2018-11-15 23:59:59', 0),
(74, 'Relaciones públicas', 7, 'Public%20Relations', 'public-relations', 11.99, 0, '2018-11-15 23:59:59', 0),
(75, 'Publicidad', 7, 'Advertising', 'advertising', 11.99, 0, '2018-11-15 23:59:59', 0),
(76, 'Marketing con vídeo y móviles', 7, 'Video%20%26%20Mobile%20Marketing', 'video-mobile-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(77, 'Merketing de contenidos', 7, 'Content%20Marketing', 'content-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(78, 'Marketing tradicional', 7, 'Non-Digital%20Marketing', 'non-digital-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(79, 'Estrategia de posicionamiento', 7, 'Growth%20Hacking', 'growth-hacking', 11.99, 0, '2018-11-15 23:59:59', 0),
(80, 'Marketing de afiliados', 7, 'Affiliate%20Marketing', 'affiliate-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(81, 'Marketing de producto', 7, 'Product%20Marketing', 'product-marketing', 11.99, 0, '2018-11-15 23:59:59', 0),
(82, 'Cursos gratis de Música', 8, 'Music', 'music-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(83, 'Instrumentos', 8, 'Instruments', 'instruments', 11.99, 0, '2018-11-15 23:59:59', 0),
(84, 'Producción', 8, 'Production', 'production', 11.99, 0, '2018-11-15 23:59:59', 0),
(85, 'Fundamentos sobre música', 8, 'Music%20Fundamentals', 'music-fundamentals', 11.99, 0, '2018-11-15 23:59:59', 0),
(86, 'Vocal', 8, 'Vocal', 'vocal', 11.99, 0, '2018-11-15 23:59:59', 0),
(87, 'Técnicas musicales', 8, 'Music%20Techniques', 'music-techniques', 11.99, 0, '2018-11-15 23:59:59', 0),
(88, 'Software de música', 8, 'Music%20Software', 'music-software', 11.99, 0, '2018-11-15 23:59:59', 0),
(89, 'Cursos gratis de Salud y fitness', 9, 'Health%20%26%20Fitness', 'health-and-fitness-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(90, 'Fitness', 9, 'Fitness', 'fitness', 11.99, 0, '2018-11-15 23:59:59', 0),
(91, 'Salud general', 9, 'General%20Health', 'general-health', 11.99, 0, '2018-11-15 23:59:59', 0),
(92, 'Deportes', 9, 'Sports', 'sports', 11.99, 0, '2018-11-15 23:59:59', 0),
(93, 'Nutrición', 9, 'Nutrition', 'nutrition', 11.99, 0, '2018-11-15 23:59:59', 0),
(94, 'Yoga', 9, 'Yoga', 'yoga', 11.99, 0, '2018-11-15 23:59:59', 0),
(95, 'Salud mental', 9, 'Mental%20Health', 'mental-health', 11.99, 0, '2018-11-15 23:59:59', 0),
(96, 'Dietética', 9, 'Dieting', 'dieting', 11.99, 0, '2018-11-15 23:59:59', 0),
(97, 'Defensa personal', 9, 'Self%20Defense', 'self-defense', 11.99, 0, '2018-11-15 23:59:59', 0),
(98, 'Seguridad y primeros auxilios', 9, 'Safety%20%26%20First%20Aid', 'safety-and-first-aid', 11.99, 0, '2018-11-15 23:59:59', 0),
(99, 'Baile', 9, 'Dance', 'dance', 11.99, 0, '2018-11-15 23:59:59', 0),
(100, 'Meditación', 9, 'Meditation', 'meditation', 11.99, 0, '2018-11-15 23:59:59', 0),
(101, 'Cursos gratis de Fotografía', 10, 'Photography', 'photography-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(102, 'Fotografía digital', 10, 'Digital%20Photography', 'digital-photography', 11.99, 0, '2018-11-15 23:59:59', 0),
(103, 'Fundamentos de fotografía', 10, 'Photography%20Fundamentals', 'photography-fundamentals', 11.99, 0, '2018-11-15 23:59:59', 0),
(104, 'Retratos', 10, 'Portraits', 'portraits', 11.99, 0, '2018-11-15 23:59:59', 0),
(105, 'Paisajes', 10, 'Landscape', 'landscape', 11.99, 0, '2018-11-15 23:59:59', 0),
(106, 'Blanco y negro', 10, 'Black%20%26%20White', 'black-and-white', 11.99, 0, '2018-11-15 23:59:59', 0),
(107, 'Herramientas de fotografía', 10, 'Photography%20Tools', 'photography-tools', 11.99, 0, '2018-11-15 23:59:59', 0),
(108, 'Fotografía móvil', 10, 'Mobile%20Photography', 'mobile-photography', 11.99, 0, '2018-11-15 23:59:59', 0),
(110, 'Fotografía comercial', 10, 'Commercial%20Photography', 'commercial-photography', 11.99, 0, '2018-11-15 23:59:59', 0),
(112, 'Fotografía de naturaleza', 10, 'Wildlife%20Photography', 'wildlife-photography', 11.99, 0, '2018-11-15 23:59:59', 0),
(113, 'Diseño de vídeo', 10, 'Video%20Design', 'video-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(114, 'Cursos gratis de Estilo de vida', 11, 'Lifestyle', 'lifestyle-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(115, 'Artes y manualidades', 11, 'Arts%20%26%20Crafts', 'arts-and-crafts', 11.99, 0, '2018-11-15 23:59:59', 0),
(116, 'Comida y bebida', 11, 'Food%20%26%20Beverage', 'food-and-beverage', 11.99, 0, '2018-11-15 23:59:59', 0),
(117, 'Belleza y maquillaje', 11, 'Beauty%20%26%20Makeup', 'beauty-and-makeup', 11.99, 0, '2018-11-15 23:59:59', 0),
(118, 'Viajes', 11, 'Travel', 'travel', 11.99, 0, '2018-11-15 23:59:59', 0),
(119, 'Juegos', 11, 'Gaming', 'gaming', 11.99, 0, '2018-11-15 23:59:59', 0),
(120, 'Bricolaje', 11, 'Home%20Improvement', 'home-improvement', 11.99, 0, '2018-11-15 23:59:59', 0),
(121, 'Cuidado y entrenamiento de mascotas', 11, 'Pet%20Care%20%26%20Training', 'pet-care-and-training', 11.99, 0, '2018-11-15 23:59:59', 0),
(122, 'Cursos gratis de Idiomas', 12, 'Language', 'language-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(123, 'Inglés', 12, 'English', 'english', 11.99, 0, '2018-11-15 23:59:59', 0),
(124, 'Español', 12, 'Spanish', 'spanish', 11.99, 0, '2018-11-15 23:59:59', 0),
(125, 'Alemán', 12, 'German', 'german', 11.99, 0, '2018-11-15 23:59:59', 0),
(126, 'Francés', 12, 'French', 'french', 11.99, 0, '2018-11-15 23:59:59', 0),
(127, 'Japonés', 12, 'Japanese', 'japanese', 11.99, 0, '2018-11-15 23:59:59', 0),
(128, 'Portugués', 12, 'Portuguese', 'portuguese', 11.99, 0, '2018-11-15 23:59:59', 0),
(129, 'Chino', 12, 'Chinese', 'chinese', 11.99, 0, '2018-11-15 23:59:59', 0),
(130, 'Ruso', 12, 'Russian', 'russian', 11.99, 0, '2018-11-15 23:59:59', 0),
(131, 'Latín', 12, 'Latin', 'latin', 11.99, 0, '2018-11-15 23:59:59', 0),
(132, 'Árabe', 12, 'Arabic', 'arabic', 11.99, 0, '2018-11-15 23:59:59', 0),
(133, 'Hebreo', 12, 'Hebrew', 'hebrew', 11.99, 0, '2018-11-15 23:59:59', 0),
(134, 'Italiano', 12, 'Italian', 'italian', 11.99, 0, '2018-11-15 23:59:59', 0),
(135, 'Cursos gratis de formación de profesorado', 13, 'Teacher%20Training', 'teacher-training-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(136, 'Diseño educativo', 13, 'Instructional%20Design', 'instructional-design', 11.99, 0, '2018-11-15 23:59:59', 0),
(137, 'Desarrollo educativo', 13, 'Educational%20Development', 'educational-development', 11.99, 0, '2018-11-15 23:59:59', 0),
(138, 'Herramientas de enseñanza', 13, 'Teaching%20Tools', 'teaching-tools', 11.99, 0, '2018-11-15 23:59:59', 0),
(139, 'Cursos gratis de Estudios Académicos', 14, 'Academics', 'academics-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(140, 'Ciencias Sociales', 14, 'Social%20Science', 'social-science', 11.99, 0, '2018-11-15 23:59:59', 0),
(141, 'Matemáticas y Ciencias', 14, 'Math%20%26%20Science', 'math-and-science', 11.99, 0, '2018-11-15 23:59:59', 0),
(142, 'Humanidades', 14, 'Humanities', 'humanities', 11.99, 0, '2018-11-15 23:59:59', 0),
(143, 'Cursos gratis para preparación de exámenes', 15, 'Test%20Prep', 'test-prep-free', 11.99, 0, '2018-11-15 23:59:59', 1),
(144, 'Examen de acceso a programas de posgrado', 15, 'Grad%20Entry%20Exam', 'grad-entry-exam', 11.99, 0, '2018-11-15 23:59:59', 0),
(145, 'Examen de acceso a la universidad', 15, 'College%20Entry%20Exam', 'college-entry-exam', 11.99, 0, '2018-11-15 23:59:59', 0),
(146, 'Técnicas de preparación para exámenes', 15, 'Test%20Taking%20Skills', 'test-taking-skills', 11.99, 0, '2018-11-15 23:59:59', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
